#include <CoreFoundation/CoreFoundation.h>
#include <CoreServices/CoreServices.h>
#include <QuickLook/QuickLook.h>

#include <Cocoa/Cocoa.h>

OSStatus GeneratePreviewForURL(void *thisInterface, QLPreviewRequestRef preview, CFURLRef url, CFStringRef contentTypeUTI, CFDictionaryRef options);
void CancelPreviewGeneration(void *thisInterface, QLPreviewRequestRef preview);

/* -----------------------------------------------------------------------------
   Generate a preview for file

   This function's job is to create preview for designated file
   ----------------------------------------------------------------------------- */

OSStatus GeneratePreviewForURL(void *thisInterface,
                               QLPreviewRequestRef preview,
                               CFURLRef url,
                               CFStringRef contentTypeUTI,
                               CFDictionaryRef options)
{
    if (QLPreviewRequestIsCancelled(preview))
        return noErr;

    //// Fetch Web Page
    
    NSData *data;
    NSError *err;
    
    /// Attempt 0: static HTML -- ok
    //NSString *html = @"<html><body><h1>It works!</h1></body></html>";
    
    /// Attempt 1: dataWithContentsOfURL -- ko
    data = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://google.com/"] options:NSDataReadingUncached error:&err];

    /// Attempt 2: NSURLConnection -- ko
    //NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://google.com/"]];
    //NSURLResponse *response;
    //data = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:&response error:&err];

    
    // Builds /Users/pete/out.txt on error
    NSString *html = [[NSString alloc] initWithData:(NSData *)data encoding:NSUTF8StringEncoding];
    [err.description writeToFile:@"/Users/pete/out.txt" atomically:YES encoding:NSUTF8StringEncoding error:nil];

    //// Display HTML
    
    NSMutableDictionary *props;
    [props setObject:@"UTF-8" forKey:(NSString *)kQLPreviewPropertyTextEncodingNameKey];
    [props setObject:@"text/html" forKey:(NSString *)kQLPreviewPropertyMIMETypeKey];

    QLPreviewRequestSetDataRepresentation(preview,
                                          (__bridge CFDataRef)[html dataUsingEncoding:NSUTF8StringEncoding],
                                          kUTTypeHTML,
                                          (__bridge CFDictionaryRef)props);
    return noErr;
}

void CancelPreviewGeneration(void *thisInterface, QLPreviewRequestRef preview)
{
    // Implement only if supported
}
