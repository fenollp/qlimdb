# A Mac OS X Quicklook plugin to know IMDB info on a file

such as:
Rate, Year, Theme, Name, a big poster, Top user review, pictures alike

# QuickLook Plugin
Well, [it appears](http://stackoverflow.com/questions/17658579/how-to-fetch-a-webpage-in-a-quicklook-plugin?noredirect=1#comment26917286_17658579)
Apple blocks HTTP access to third parties' ql plugins.

# Further
In order to not let the project die, I will write a tool that adds the .weblock file
to IMDB from each movie found on given folders. Seems more portable even.
